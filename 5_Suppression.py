import psycopg2

def supprimerExemplaire(conn, ide):
    cur = conn.cursor()
    supprimerPret(conn, ide)
    sql = "DELETE FROM Exemplaire WHERE id = %i;" %ide
    try:
        cur.execute(sql)
        conn.commit()
        print("\nElement supprimé")
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Suppression annulée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Suppression annulée")
        print("Message système :", e)
        conn.rollback()
        return
        
def supprimerPret(conn, ide):
    cur = conn.cursor() 
    sql = "DELETE FROM Pret WHERE id = %i" %ide
    try:
        cur.execute(sql)
        conn.commit()
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Suppression annulée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Suppression annulée")
        print("Message système :", e)
        conn.rollback()
        return

def supprimerExemplaire2(conn, code):
    cur = conn.cursor()
    sql = "SELECT ID FROM Exemplaire WHERE ressource = %i" %code
    try:
        cur.execute(sql)
        raw = cur.fetchone()
        while raw:
            ide = raw[0]
            supprimerPret(conn, ide)
            raw = cur.fetchone()
        sql2 = "DELETE FROM Exemplaire WHERE ressource = %i" %code
        try:
            cur.execute(sql2)
            conn.commit()
        except psycopg2.DataError as e:
            conn.rollback()
            print("\n   Suppression annulée")
            print("Message système :", e)
            return
        except psycopg2.IntegrityError as e:
            print("\n   Suppression annulée")
            print("Message système :", e)
            conn.rollback()
            return
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Suppression annulée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Suppression annulée")
        print("Message système :", e)
        conn.rollback()
        return

def supprimerActeur(conn, code):
    cur = conn.cursor() 
    sql = "DELETE FROM Acteur WHERE code = %i" %code
    try:
        cur.execute(sql)
        conn.commit()
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Suppression annulée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Suppression annulée")
        print("Message système :", e)
        conn.rollback()
        return

def supprimerRealisateur(conn, code):
    cur = conn.cursor() 
    sql = "DELETE FROM Realisateur WHERE code = %i" %code
    try:
        cur.execute(sql)
        conn.commit()
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Suppression annulée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Suppression annulée")
        print("Message système :", e)
        conn.rollback()
        return

def supprimerAuteur(conn, code):
    cur = conn.cursor() 
    sql = "DELETE FROM Ecrivain WHERE code = %i" %code
    try:
        cur.execute(sql)
        conn.commit()
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Suppression annulée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Suppression annulée")
        print("Message système :", e)
        conn.rollback()
        return

def supprimerCompositeur(conn, code):
    cur = conn.cursor() 
    sql = "DELETE FROM Compositeur WHERE code = %i" %code
    try:
        cur.execute(sql)
        conn.commit()
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Suppression annulée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Suppression annulée")
        print("Message système :", e)
        conn.rollback()
        return

def supprimerInterprete(conn, code):
    cur = conn.cursor() 
    sql = "DELETE FROM Interprete WHERE code = %i" %code
    try:
        cur.execute(sql)
        conn.commit()
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Suppression annulée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Suppression annulée")
        print("Message système :", e)
        conn.rollback()
        return


def supprimerFilm(conn, code):
    supprimerExemplaire2(conn, code) 
    supprimerRealisateur(conn, code)
    supprimerActeur(conn, code)
    cur = conn.cursor()
    sql = "DELETE FROM Film WHERE code = %i" %code
    try:
        cur.execute(sql)
        conn.commit()
        sql2 = "DELETE FROM Ressource WHERE code = %i" %code
        try:
            cur.execute(sql2)
            conn.commit()
        except psycopg2.DataError as e:
            conn.rollback()
            print("\n   Suppression annulée")
            print("Message système :", e)
            return
        except psycopg2.IntegrityError as e:
            print("\n   Suppression annulée")
            print("Message système :", e)
            conn.rollback()
            return
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Suppression annulée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Suppression annulée")
        print("Message système :", e)
        conn.rollback()
        return

def supprimerLivre(conn, code):
    supprimerExemplaire2(conn, code) 
    supprimerAuteur(conn, code)
    cur = conn.cursor()
    sql = "DELETE FROM Livre WHERE code = %i" %code
    try:
        cur.execute(sql)
        conn.commit()
        sql2 = "DELETE FROM Ressource WHERE code = %i" %code
        try:
            cur.execute(sql2)
            conn.commit()
        except psycopg2.DataError as e:
            conn.rollback()
            print("\n   Suppression annulée")
            print("Message système :", e)
            return
        except psycopg2.IntegrityError as e:
            print("\n   Suppression annulée")
            print("Message système :", e)
            conn.rollback()
            return
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Suppression annulée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Suppression annulée")
        print("Message système :", e)
        conn.rollback()
        return

def supprimerEnregistrement(conn, code):
    supprimerExemplaire2(conn, code) 
    supprimerCompositeur(conn, code)
    supprimerInterprete(conn, code)
    cur = conn.cursor()
    sql = "DELETE FROM EnregistrementMusical WHERE code = %i" %code
    try:
        cur.execute(sql)
        conn.commit()
        sql2 = "DELETE FROM Ressource WHERE code = %i" %code
        try:
            cur.execute(sql2)
            conn.commit()
        except psycopg2.DataError as e:
            conn.rollback()
            print("\n   Suppression annulée")
            print("Message système :", e)
            return
        except psycopg2.IntegrityError as e:
            print("\n   Suppression annulée")
            print("Message système :", e)
            conn.rollback()
            return
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Suppression annulée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Suppression annulée")
        print("Message système :", e)
        conn.rollback()
        return
