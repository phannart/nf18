##Importations

import psycopg2
import Ajout
import Afficher
import Suppression
import Update

##Connexion

conn = psycopg2.connect("dbname='dbnf18p123' user='nf18p123' host='tuxa.sme.utc' password='BhTZ2uJr'")

##Menu

choice = 1
while choice != 0:
    print ("\n\n1)    Pour afficher des données sur les ressources, entrez 1\n") #Fait
    print ("2)    Pour ajouter une ressource ou un exemplaire, entrez 2\n") #Fait
    print ("3)    Pour modifier une ressource entrez 3\n") 
    print ("4)    Pour supprimer des elements de la base de donnée, entrez 4\n") #Fait
    print ("5)    Pour visualiser les statistiques des ressources, entrez 5\n") #Fait
    print ("6)    Pour ajouter un utilisateur, entrez 6\n") #Fait
    print ("7)    Pour traiter un pret ou une sanction, entrez 7\n") #Fait
    print ("8)    Pour modifier un utilisateur, entrez 8\n") 
    print ("9)    Pour afficher les utilisateurs, entrez 9\n") #Fait

    print ("    Pour sortir, entrez 0\n")

    while True:
        try:
            choice = int(input("Votre choix : "))
            if(choice<0 or choice>9):
                print("--Le chiffre n'est pas compris entre 0 et 9 veuillez reéssayer--")
                continue
            else:
                break
        except:
            print("--Veuillez renseigner un entier--")
            continue


    if choice == 1:
        print ("\n    1) Pour afficher les données liées aux Films, entrez 1\n")
        print ("    2) Pour afficher les données liées aux livres, entrez 2\n")
        print ("    3) Pour afficher les données liées aux enregistrements, entrez 3\n")
        print ("    4) Pour afficher les exemplaires disponilbes de la ressource choisie, entrez 4\n")
        print ("    5) Pour afficher toutes les ressources, entrez 5\n")

        while True:
            try:
                choice2 = int(input("\n    Votre choix : "))
                if(choice2<1 or choice2>5):
                    print("    --Le chiffre n'est pas compris entre 1 et 5 veuillez reéssayer--")
                    continue
                else:
                    break
            except:
                print("    --Veuillez renseigner un entier--")
                continue
       
        print("\n")
        if choice2 == 1:
            Afficher.afficherFilm(conn)
        if choice2 == 2:
            Afficher.afficherLivre(conn)
        if choice2 == 3:
            Afficher.afficherEnregistrement(conn)
        if choice2 == 4:
            Afficher.afficherDispo(conn)
        if choice2 == 5:
            print("\n")
            Afficher.AfficherRessource(conn)


    if choice == 2:
        print ("\n    1) Pour ajouter un Film, entrez 1\n")
        print ("    2) Pour ajouter un livre, entrez 2\n")
        print ("    3) Pour ajouter un enregistrement, entrez 3\n")
        print ("    4) Pour ajouter un exemplaire, entrez 4\n")
        while True:
            try:
                choice2 = int(input("\n    Votre choix : "))
                if(choice2<1 or choice2>4):
                    print("    --Le chiffre n'est pas compris entre 1 et 4 veuillez reéssayer--")
                    continue
                else:
                    break
            except:
                print("    --Veuillez renseigner un entier--")
                continue
        print("\n")
        if choice2 == 1:
                Ajout.ajoutFilm(conn)
        if choice2 == 2:
                Ajout.ajoutLivre(conn)
        if choice2 == 3:
                Ajout.ajoutEnregistrement(conn)
        if choice2 == 4:
                Ajout.ajoutExemplaire(conn)
        
    if choice == 3:
        print ("\n    1) Pour modifier un Film, entrez 1\n")
        print ("    2) Pour modifier un livre, entrez 2\n")
        print ("    3) Pour modifier un enregistrement, entrez 3\n")
        while True:
            try:
                choice2 = int(input("\n    Votre choix : "))
                if(choice2<1 or choice2>3):
                    print("    --Le chiffre n'est pas compris entre 1 et 3 veuillez reéssayer--")
                    continue
                else:
                    break
            except:
                print("    --Veuillez renseigner un entier--")
                continue
        print("\n")
        if choice2 == 1:
                Update.modifFilm(conn)
        if choice2 == 2:
                Update.modifLivre(conn)
        if choice2 == 3:
                Update.modifEnregistrement(conn)

    if choice == 4:
        print ("\n    1) Pour supprimer un Film, entrez 1")
        print ("    2) Pour supprimer un livre, entrez 2")
        print ("    3) Pour supprimer un enregistrement, entrez 3")
        print ("    4) Pour supprimer un exemplaire, entrez 4")
        while True:
            try:
                choice2 = int(input("\n    Votre choix : "))
                if(choice2<1 or choice2>5):
                    print("    --Le chiffre n'est pas compris entre 0 et 5 veuillez reéssayer--")
                    continue
                else:
                    break
            except:
                print("    --Veuillez renseigner un entier--")
                continue
        print("\n")
        if choice2 == 1:
            code = int ( input( "Entrez le code du film à supprimer : "))
            Suppression.supprimerFilm(conn, code)
        if choice2 == 2:
            code = int ( input( "Entrez le code du livre à supprimer : "))
            Suppression.supprimerLivre(conn, code)
        if choice2 == 3:
            code = int ( input( "Entrez le code de l'enregistrement à supprimer : "))
            Suppression.supprimerEnregistrement(conn, code)
        if choice2 == 4:
            ide = int(input( "Entrez l'identifiant de l'exemplaire à supprimer : "))
            Suppression.supprimerExemplaire(conn,ide)

       
    if choice == 5:
        print ("\n    1) Pour visualiser le classement des ressources empruntées de la plus empruntée à la moins empruntée, entrez 1\n")
        print ("    2) Pour visualiser le classement des ressources empruntées de la plus empruntée à la moins empruntée pour une periode donnée, entrez 2\n")
        while True:
            try:
                choice2 = int(input("\n    Votre choix : "))
                if(choice2<1 or choice2>2):
                    print("    --Le chiffre n'est pas compris entre 1 et 2 veuillez reéssayer--")
                    continue
                else:
                    break
            except:
                print("    --Veuillez renseigner un entier--")
                continue
        print("\n")
        if choice2 == 1:
            Afficher.stat(conn)
        if choice2 == 2:
            Afficher.statperiode(conn)
        

    if choice == 6:
        print ("\n    1) Pour ajouter un membre, entrez 1\n")
        print ("    2) Pour sajouter un adherent, entrez 2\n")
        while True:
            try:
                choice2 = int(input("\n    Votre choix : "))
                if(choice2<1 or choice2>2):
                    print("    --Le chiffre n'est pas compris entre 1 et 2 veuillez reéssayer--")
                    continue
                else:
                    break
            except:
                print("    --Veuillez renseigner un entier--")
                continue

        if choice2 == 1:
            Ajout.ajoutMembre(conn)
        if choice2 == 2:
            Ajout.ajoutAdherent(conn)

    if choice == 7:
        print ("\n    1) Pour ajouter un pret, entrez 1\n")
        print ("    2) Pour cloturer un pret, entrez 2\n")
        print ("    3) Pour ajouter une sanction liée à un pret, entrez 3\n")
        print ("    4) Pour afficher les prets et eventuelles sanctions lié à un adhèrent, entrez 4\n")
        while True:
            try:
                choice2 = int(input("\n    Votre choix : "))
                if(choice2<1 or choice2>4):
                    print("    --Le chiffre n'est pas compris entre 1 et 4 veuillez reéssayer--")
                    continue
                else:
                    break
            except:
                print("    --Veuillez renseigner un entier--")
                continue
        if choice2 == 1:
            Ajout.ajoutPret(conn)
        if choice2 == 2:
            Update.cloturePret(conn)
        if choice2 == 3:
            Update.Sanction(conn)
        if choice2 == 4:
            Afficher.AfficherPretMembre(conn)
    
    if choice == 8:
        print ("\n    1) Pour modifier un membre, entrez 1\n")
        print ("    2) Pour modifier un adherent, entrez 2\n")
        while True:
            try:
                choice2 = int(input("\n    Votre choix : "))
                if(choice2<1 or choice2>2):
                    print("    --Le chiffre n'est pas compris entre 1 et 2 veuillez reéssayer--")
                    continue
                else:
                    break
            except:
                print("    --Veuillez renseigner un entier--")
                continue
        if choice2 == 1:
            Update.modifMembre(conn)
        if choice2 == 2:
            Update.modifAdherent(conn)

    
    if choice == 9:
        print ("\n    1) Pour afficher les membres, entrez 1\n")
        print ("    2) Pour afficher les adherents, entrez 2\n")
        while True:
            try:
                choice2 = int(input("\n    Votre choix : "))
                if(choice2<1 or choice2>2):
                    print("    --Le chiffre n'est pas compris entre 1 et 2 veuillez reéssayer--")
                    continue
                else:
                    break
            except:
                print("    --Veuillez renseigner un entier--")
                continue
        if choice2 == 1:
            Afficher.AfficherMembre(conn)
        if choice2 == 2:
            Afficher.AfficherAdherent(conn)



conn.close()

