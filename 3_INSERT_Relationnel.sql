DELETE FROM Pret;
DELETE FROM Exemplaire;
DELETE FROM Adherent;
DELETE FROM Membre;
DELETE FROM CompteUtilisateur;
DELETE FROM Utilisateur;
DELETE FROM Realisateur;
DELETE FROM Ecrivain;
DELETE FROM Compositeur;
DELETE FROM Interprete;
DELETE FROM Acteur;
DELETE FROM Contributeur ; 
DELETE FROM Film;
DELETE FROM Livre;
DELETE FROM EnregistrementMusical;
DELETE FROM Ressource;

INSERT INTO Ressource VALUES (1, 'la richesse des nations',TO_DATE('17760401','YYYYMMDD'), 'william wtrahan', 'wconomie',  123 );
INSERT INTO Ressource VALUES (2, 'vingt Mille Lieues sous les mers', TO_DATE('18691231','YYYYMMDD'), 'pierre-jules hetzel', 'aventure', 321);
INSERT INTO Ressource VALUES (3, 'star wars',TO_DATE('19990516','YYYYMMDD'), 'presses de la cité', 'science-fiction',  147 );
INSERT INTO Ressource VALUES (4, 'harry potter',TO_DATE('19970626','YYYYMMDD'), 'gallimard jeunesse', 'fantaisie',  187 );
INSERT INTO Ressource VALUES (5, 'les misérables',TO_DATE('18620115','YYYYMMDD'), 'albert lacoix', 'roman',  197 );
INSERT INTO Ressource VALUES (6, 'symphonie de beethoven',TO_DATE('20000626','YYYYMMDD'), 'musiqueclassique', 'classique',  777 );
INSERT INTO Ressource VALUES (7, 'la flute enchantée',TO_DATE('20000626','YYYYMMDD'), 'musiqueclassique', 'classique',  788 );
INSERT INTO Ressource VALUES (8, 'requiem de mozart',TO_DATE('20000626','YYYYMMDD'), 'musiqueclassique', 'classique',  789 );
INSERT INTO Ressource VALUES (9, 'willow',TO_DATE('19880516','YYYYMMDD'), 'cinema fantasy', 'science-fiction',  157 );

INSERT INTO Livre VALUES (1, 17544, 'Recherches sur la nature et les causes de la richesse des nations (en anglais, An Inquiry into the Nature and Causes of the Wealth of Nations) ou plus simplement la Richesse des nations est le plus célèbre ouvrage d’Adam Smith. Publié en 1776, c’est le premier livre moderne d’économie.', 'francais');
INSERT INTO Livre VALUES (2, 12340, 'Relate le voyage de trois naufragés capturés par le capitaine Nemo, mystérieux inventeur qui parcourt les fonds des mers à bord du Nautilus un sous-marin très en avance sur les technologies de l"époque', 'anglais');
INSERT INTO Livre VALUES (4, 48756, 'histoire, se situant dans les années 1990, raconte la jeunesse de Harry Potter, sorcier orphelin élevé sans affection ni considération par la seule famille vivante qui lui reste : son oncle et sa tante moldus (sans pouvoirs magiques). Le garçon découvre son identité de sorcier, son héritage tragique et la responsabilité qui lui revient', 'anglais');
INSERT INTO Livre VALUES (3, 14798, 'Relate le voyage de trois naufragés capturés par le capitaine Nemo, mystérieux inventeur qui parcourt les fonds des mers à bord du Nautilus, un sous-marin très en avance sur les technologies de l"époque', 'français');

INSERT INTO EnregistrementMusical VALUES (6,'15:02:00');
INSERT INTO EnregistrementMusical VALUES (7,'00:03:36');
INSERT INTO EnregistrementMusical VALUES (8,'00:05:36');

INSERT INTO Film VALUES (3,'L"histoire de Star Wars se déroule dans une galaxie qui est le théâtre d"affrontements entre les Chevaliers Jedi et les Seigneurs noirs des Sith, personnes sensibles à la Force, un champ énergétique mystérieux leur procurant des pouvoirs psychiques. Les Jedi maîtrisent le Côté lumineux de la Force, pouvoir bénéfique et défensif, pour maintenir la paix dans la galaxie. Les Sith utilisent le Côté obscur, pouvoir nuisible et destructeur, pour leurs usages personnels et pour dominer la galaxie','01:36:00', 'français');
INSERT INTO Film VALUES (9,'Une prophétie annonce qu"une princesse verra le jour, qui mettra fin au règne tyrannique de Bavmorda, la reine maléfique des Daïkinis (les humains). Apprenant la naissance imminente d"Élora, l"élue, Bavmorda s"empresse de donner l"ordre à ses guerriers de regrouper toutes les femmes enceintes du royaume pour empêcher l"accomplissement de la prophétie','02:28:50', 'anglais');

INSERT INTO Contributeur VALUES (1, 'verne', 'jules', TO_DATE('18280208','YYYYMMDD'), 'français');
INSERT INTO Contributeur VALUES (2, 'mozart', 'johannes', TO_DATE('17560127','YYYYMMDD'), 'autrichien');
INSERT INTO Contributeur VALUES (3, 'beethoven', 'ludwif', TO_DATE('17701109','YYYYMMDD'), 'allemand');
INSERT INTO Contributeur VALUES (4, 'fisher', 'carrie', TO_DATE('19901109','YYYYMMDD'), 'anglais');
INSERT INTO Contributeur VALUES (5, 'hamill', 'mark', TO_DATE('19951109','YYYYMMDD'), 'anglais');
INSERT INTO Contributeur VALUES (6, 'guinness', 'alec', TO_DATE('19401109','YYYYMMDD'), 'anglais');
INSERT INTO Contributeur VALUES (7, 'lucas', 'george', TO_DATE('19440611','YYYYMMDD'), 'anglais');
INSERT INTO Contributeur VALUES (8, 'howard', 'ron', TO_DATE('19661221','YYYYMMDD'), 'anglais');


INSERT INTO Compositeur VALUES (2,7);
INSERT INTO Compositeur VALUES (2,8);
INSERT INTO Compositeur VALUES (3,6);

INSERT INTO Interprete VALUES (2,7);
INSERT INTO Interprete VALUES (2,8);
INSERT INTO Interprete VALUES (3,7);
INSERT INTO Interprete VALUES (3,6);

INSERT INTO Acteur VALUES (4,3);
INSERT INTO Acteur VALUES (5,3);
INSERT INTO Acteur VALUES (6,3);

INSERT INTO Realisateur VALUES (6,3);
INSERT INTO Realisateur VALUES (6,9);
INSERT INTO Realisateur VALUES (8,9);


INSERT INTO Ecrivain VALUES (1, 2);
INSERT INTO Ecrivain VALUES (4, 1);
INSERT INTO Ecrivain VALUES (5, 3);
INSERT INTO Ecrivain VALUES (7, 4);
INSERT INTO Ecrivain VALUES (5, 4);

INSERT INTO Utilisateur VALUES ('richard.duchateau@bibli-compiegne.fr', 'duchateau', 'richard', TO_DATE('19550914','YYYYMMDD'), '30 Rue des Peupliers 60200 Compiègne');
INSERT INTO Utilisateur VALUES ('richard.duchateau@laposte.fr', 'duchateau', 'richard', TO_DATE('19560914','YYYYMMDD'), '30 Rue des Peupliers 60200 Compiègne');
INSERT INTO Utilisateur VALUES ('genevieve.ricard@gmail.com', 'ricard', 'geneviève', TO_DATE('19491109','YYYYMMDD'), '87 Avenue de la Liberté 59000 Lille');
INSERT INTO Utilisateur VALUES ('marie.dupon@ville-compiegne.fr', 'dupon', 'marie', TO_DATE('20050910','YYYYMMDD'), '30 Rue du Port à Bateaux 60200 Compiègne');
INSERT INTO Utilisateur VALUES ('mathis.tivenin@laposte.fr', 'tivenin', 'mathis', TO_DATE('19440708','YYYYMMDD'), '12 allée des Pin 60200 Compiègne');
INSERT INTO Utilisateur VALUES ('paul.russel@gmail.com', 'russel', 'paul', TO_DATE('20001109','YYYYMMDD'), '87 Avenue de la Mare Gaudry 60200 Compiègne');

INSERT INTO CompteUtilisateur VALUES ('riducha','Xxx','richard.duchateau@bibli-compiegne.fr', 'duchateau', 'richard');
INSERT INTO CompteUtilisateur VALUES ('riducha2','XxUx','richard.duchateau@laposte.fr', 'duchateau', 'richard');
INSERT INTO CompteUtilisateur VALUES ('gericar','XxOOx','genevieve.ricard@gmail.com', 'ricard', 'geneviève');
INSERT INTO CompteUtilisateur VALUES ('madupon','XxPOx','marie.dupon@ville-compiegne.fr', 'dupon', 'marie');
INSERT INTO CompteUtilisateur VALUES ('mathive','XxJUx','mathis.tivenin@laposte.fr', 'tivenin', 'mathis');
INSERT INTO CompteUtilisateur VALUES ('parusse','XxHGVx','paul.russel@gmail.com', 'russel', 'paul');


INSERT INTO Membre VALUES ('richard.duchateau@bibli-compiegne.fr', 'duchateau', 'richard');

INSERT INTO Adherent VALUES ('richard.duchateau@laposte.fr', 'duchateau', 'richard', '1', NULL, '1', 0667897621);
INSERT INTO Adherent VALUES ('mathis.tivenin@laposte.fr',  'tivenin', 'mathis', '1', NULL, '1', 0667866627);
INSERT INTO Adherent VALUES ('paul.russel@gmail.com', 'russel', 'paul', '1', NULL , '1', 0669999641);
INSERT INTO Adherent VALUES ('marie.dupon@ville-compiegne.fr', 'dupon', 'marie', '1', NULL, '1', 0688888891);
INSERT INTO Adherent VALUES ('genevieve.ricard@gmail.com', 'ricard', 'geneviève', '1', NULL , '1', 0678542209);

INSERT INTO Exemplaire VALUES (1,'3.44','neuf','1',1);
INSERT INTO Exemplaire VALUES (2,'2','abime','1',1);
INSERT INTO Exemplaire VALUES (3,'3.44','neuf','0',1);
INSERT INTO Exemplaire VALUES (4,'3','bon','1',1);
INSERT INTO Exemplaire VALUES (5,'3.44','neuf','0',2);
INSERT INTO Exemplaire VALUES (6,'3.44','neuf','1',2);
INSERT INTO Exemplaire VALUES (7,'2','bon','0',3);
INSERT INTO Exemplaire VALUES (8,'3.44','neuf','1',3);
INSERT INTO Exemplaire VALUES (9,'3','bon','1',4);
INSERT INTO Exemplaire VALUES (10,'3.44','neuf','1',4);
INSERT INTO Exemplaire VALUES (11,'3','bon','1',4);
INSERT INTO Exemplaire VALUES (12,'3.44','neuf','1',4);
INSERT INTO Exemplaire VALUES (13,'3.44','neuf','1',6);
INSERT INTO Exemplaire VALUES (14,'2','bon','1',6);
INSERT INTO Exemplaire VALUES (15,'3.44','neuf','1',6);
INSERT INTO Exemplaire VALUES (16,'3','bon','1',7);
INSERT INTO Exemplaire VALUES (17,'3.44','neuf','0',7);
INSERT INTO Exemplaire VALUES (18,'3.44','neuf','0',7);
INSERT INTO Exemplaire VALUES (19,'2','abime','1',7);
INSERT INTO Exemplaire VALUES (20,'3.44','neuf','1',3);
INSERT INTO Exemplaire VALUES (21,'3','bon','1',3);
INSERT INTO Exemplaire VALUES (22,'3.44','neuf','1',8);
INSERT INTO Exemplaire VALUES (23,'14','bon','1',9);
INSERT INTO Exemplaire VALUES (24,'14','neuf','0',9);
INSERT INTO Exemplaire VALUES (25,'14','neuf','0',9);

INSERT INTO Pret VALUES (1,TO_DATE('20200914','YYYYMMDD'),'paul.russel@gmail.com','russel','paul',25,'retard',21,NULL,NULL,'0');
INSERT INTO Pret VALUES (3,TO_DATE('20200914','YYYYMMDD'),'paul.russel@gmail.com','russel','paul',25,'endommagement',NULL,'abime',2,'0');
INSERT INTO Pret VALUES (4,TO_DATE('20190914','YYYYMMDD'),'paul.russel@gmail.com','russel','paul',25,NULL, NULL, NULL, NULL,'0');
INSERT INTO Pret VALUES (5,TO_DATE('20210524','YYYYMMDD'),'paul.russel@gmail.com','russel','paul',25,NULL, NULL, NULL, NULL,'1');
INSERT INTO Pret VALUES (5,TO_DATE('20170914','YYYYMMDD'),'paul.russel@gmail.com','russel','paul',25,NULL, NULL, NULL, NULL,'0');
INSERT INTO Pret VALUES (14,TO_DATE('20210914','YYYYMMDD'),'richard.duchateau@laposte.fr','duchateau','richard',25,NULL, NULL, NULL, NULL,'0');
INSERT INTO Pret VALUES (4,TO_DATE('20180914','YYYYMMDD'),'richard.duchateau@laposte.fr','duchateau','richard',25,NULL, NULL, NULL, NULL,'0');
INSERT INTO Pret VALUES (24,TO_DATE('20210524','YYYYMMDD'),'richard.duchateau@laposte.fr','duchateau','richard',25,NULL, NULL, NULL, NULL,'1');
INSERT INTO Pret VALUES (25,TO_DATE('20180914','YYYYMMDD'),'richard.duchateau@laposte.fr','duchateau','richard',25,'lesdeux',45,'perdu',14,'0');
INSERT INTO Pret VALUES (3,TO_DATE('20210504','YYYYMMDD'),'marie.dupon@ville-compiegne.fr','dupon','marie',25,NULL, NULL, NULL, NULL,'1');
INSERT INTO Pret VALUES (17,TO_DATE('20210512','YYYYMMDD'),'marie.dupon@ville-compiegne.fr','dupon','marie',25,NULL, NULL, NULL, NULL,'1');
INSERT INTO Pret VALUES (7,TO_DATE('20210505','YYYYMMDD'),'marie.dupon@ville-compiegne.fr','dupon','marie',25,NULL, NULL, NULL, NULL,'1');
INSERT INTO Pret VALUES (18,TO_DATE('20210512','YYYYMMDD'),'marie.dupon@ville-compiegne.fr','dupon','marie',25,NULL, NULL, NULL, NULL,'1');

UPDATE Exemplaire SET etat='perdu',dispo = '0' where id= 25;
UPDATE Adherent SET date_avant_nouvel_emprunt=TO_DATE('20210524','YYYYMMDD'), droit_pret='0' where mail='paul.russel@gmail.com' AND nom='russel' AND prenom='paul';
