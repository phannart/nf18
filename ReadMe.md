<H1>Sujet : Gestion d'une bibliothèque/ Subject: Managing a library </H1>

<H2>Francais : </H2>

Vous êtes chargés de concevoir un système de gestion pour une bibliothèque municipale qui souhaite informatiser ses activités : catalogage, consultations, gestion des utilisateurs, prêts, etc.

La bibliothèque offre un accès à un large choix de ressources de différents types (livres, films, et enregistrement musicaux). Une ressource, quelque soit son type, a un code unique, un titre, une liste de contributeurs, une date d'apparition, un éditeur, un genre et un code de classification qui permet de la localiser dans la bibliothèque. Un contributeur est caractérisé par son nom, son prénom, sa date de naissance et sa nationalité. Dans le cas d'un livre, les contributeurs sont les auteurs du document. Dans le cas d'une œuvre musicale, on distinguera compositeurs et interprètes. De même, on distinguera les réalisateurs et les acteurs pour les films. On souhaite également conserver des informations spécifiques suivant le type du document, par exemple : l'ISBN d'un livre et son résumé, la langue des documents écrits et des films, la longueur d'un film ou d'une œuvre musicale, le synopsis d'un film, etc. Enfin, les ressources dont dispose la bibliothèque peuvent être disponibles en plusieurs exemplaires, chacun dans un état différent : neuf, bon, abîmé ou perdu.

Chaque membre du personnel de la bibliothèque dispose d'un compte utilisateur (login et mot de passe) qui lui permet d'accéder aux fonctions d'administration du système. Chaque membre est caractérisé par son nom, son prénom, son adresse et son adresse e-mail.
Les adhérents de la bibliothèque disposent, eux aussi, d'un compte utilisateur (login et mot de passe) ainsi que d'une carte d'adhérent qui leur permettent d'emprunter des documents. Un adhérent est caractérisé par son nom, prénom, date de naissance, adresse, adresse e-mail et numéro de téléphone. La bibliothèque souhaite garder trace de toutes les adhésions, actuelles et passées.

Pour pouvoir emprunter un document, un adhérent à besoin de s'authentifier. Chaque prêt est caractérisé par une date de prêt et une durée de prêt. Un document ne peut être emprunté que s'il est disponible et en bon état. Un adhèrent ne peut emprunter simultanément qu'un nombre limité d'œuvres, chacune pour une durée limitée. Un adhérent sera sanctionné pour les retards dans le retour d'un ouvrage, ainsi que s'il dégrade l'état de celui-ci. Tout retard dans la restitution des documents empruntés entraîne une suspension du droit de prêt d'une durée égale au nombre de jours de retard. En cas de perte ou détérioration grave d'un document, la suspension du droit de prêt est maintenue jusqu'à ce que l'adhérent rembourse le document. Enfin, la bibliothèque peut choisir de blacklister un adhérent en cas de sanctions répétées.

Besoins :
- Faciliter aux adhérents la recherche de documents et la gestion de leurs emprunts.
- Faciliter la gestion des ressources documentaires : ajouter des documents, modifier leur description, ajouter des exemplaires d'un document, etc.
- Faciliter au personnel la gestion des prêts, des retards et des réservation.
- Faciliter la gestion des utilisateurs et de leurs données.
- Établir des statistiques sur les documents empruntés par les adhérents, cela permettra par exemple d'établir la liste des documents populaires, mais aussi d'étudier le profil des adhérents pour pouvoir leur suggérer des documents.

<H2>English : </H2>

You are responsible for designing a management system for a municipal library that wishes to computerize its activities: cataloguing, consultations, user management, loans, etc.

The library offers access to a wide choice of resources of different types (books, films, and musical recordings). A resource, whatever its type, has a unique code, a title, a list of contributors, a date of appearance, a publisher, a genre and a classification code which allows it to be located in the library. A contributor is characterized by his surname, first name, date of birth and nationality. In the case of a book, the contributors are the authors of the document. In the case of a musical work, a distinction will be made between composers and performers. Similarly, a distinction will be made between directors and actors for films. We also want to keep specific information depending on the type of document, for example: the ISBN of a book and its summary, the language of written documents and films, the length of a film or a musical work, the synopsis of a film, etc. Finally, the resources available to the library may be available in several copies, each in a different state: new, good, damaged or lost.

Each library staff member has a user account (login and password) which allows them to access the system administration functions. Each member is characterized by his surname, first name, address and e-mail address.
Library members also have a user account (login and password) as well as a membership card which allows them to borrow documents. A member is characterized by his surname, first name, date of birth, address, e-mail address and telephone number. The library wishes to keep track of all memberships, current and past.

To be able to borrow a document, a member needs to authenticate. Each loan is characterized by a loan date and a loan duration. A document can only be borrowed if it is available and in good condition. A member can only borrow a limited number of works at the same time, each for a limited period. A member will be penalized for delays in the return of a book, as well as if he deteriorates the condition of it. Any delay in returning the borrowed documents entails a suspension of the lending right for a period equal to the number of days of delay. In the event of loss or serious deterioration of a document, the suspension of the lending right is maintained until the member reimburses the document. Finally, the library can choose to blacklist a member in the event of repeated sanctions.

Needs :
- Make it easier for members to find documents and manage their loans.
- Facilitate the management of documentary resources: add documents, modify their description, add copies of a document, etc.
- Make it easier for staff to manage loans, delays and reservations.
- Facilitate the management of users and their data.
- Establish statistics on the documents borrowed by the members, this will allow for example to establish the list of popular documents, but also to study the profile of the members in order to be able to suggest them documents.
