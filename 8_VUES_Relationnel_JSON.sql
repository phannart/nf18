DROP VIEW IF EXISTS Adresses_Sanctions,Logi,VILLE,Adresse,Retard,Vue_Sanctions, Nombre_Voisins;


/* Affiche toutes les sanctions de ceux qui en ont ou qui en ont eu */

CREATE VIEW Vue_Sanctions AS
SELECT nom, 
prenom, 
mail,      
t_sanction ->> 'type' AS type_sanction,
CAST(t_sanction ->>'nb_jour' AS INTEGER) AS nombre_jours,
t_sanction ->> 'nv_etat' AS nouveau_etat,
CAST(t_sanction ->>'a_payer' AS INTEGER) AS somme_a_payer,
actuel
FROM Pret;



/*Affiche si les personnes sanctionnées ont un retard en cours*/

CREATE VIEW Retard AS
SELECT nom,
prenom,
mail,
CASE WHEN type_sanction='retard' OR type_sanction='lesdeux'
    THEN 1
    ELSE 0
  END AS retard
FROM Vue_Sanctions V
Where actuel = '1';



/* Affiche toutes les adresses de la base de donnée avec l'habitant de cette adresse */

CREATE VIEW Adresse AS
SELECT nom, 
prenom, 
ad.*
FROM Utilisateur U, JSON_ARRAY_ELEMENTS(U.adresse) ad;



/* Affiche les villes des habitants */   

CREATE VIEW VILLE AS
SELECT nom, 
prenom, 
ad->>'ville' AS ville
FROM Utilisateur U JOIN JSON_ARRAY_ELEMENTS(U.adresse) ad ON TRUE;



/* Affiche les login des adherents */

CREATE VIEW Logi AS
SELECT nom, 
prenom,
compteUtilisateur->>'logi' AS logi
FROM Utilisateur;



/* Affiche les adresses des personnes qui ont une sanction actuelle pour envoyer une lettre par exemple*/

CREATE VIEW Adresses_Sanctions AS
SELECT U.nom, 
U.prenom,       
t_sanction ->> 'type' AS type_sanction,
CAST(t_sanction ->>'nb_jour' AS INTEGER) AS nombre_jours,
t_sanction ->> 'nv_etat' AS nouveau_etat,
CAST(t_sanction ->>'a_payer' AS INTEGER) AS somme_a_payer,
U.adresse
FROM Pret P
INNER JOIN Utilisateur U ON U.nom = P.nom AND U.prenom = P.prenom AND U.mail = P.mail
WHERE P.actuel = '1';


/* Affiche le nombre de personnes habitant dans la même rue sauf rue où une seule personne habite */


CREATE VIEW Nombre_Voisins AS
SELECT adresse->>'rue' AS rue, COUNT(nom)
FROM Utilisateur
GROUP BY rue
HAVING COUNT(nom) > 1;  
