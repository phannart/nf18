/* En combien d'exemplaires existe une ressource donnée (ici la ressource 1) */
SELECT COUNT(*) AS nb_exemplaires_ressource1
FROM Exemplaire
WHERE Exemplaire.ressource = 1;

/* En combien d'exemplaires disponibles existe une ressource donnée (ici la ressource 1) */
SELECT COUNT(*) AS nb_exemplaires_dispo_ressource1
FROM Exemplaire
WHERE Exemplaire.ressource = 1 AND Exemplaire.dispo = '1';

/* Les identifiants des exemplaires disponibles pour une ressource donnée (ici la ressource 1) */
SELECT id AS id_exemplaires_dispo_ressource1
FROM Exemplaire
WHERE Exemplaire.ressource = 1 AND Exemplaire.dispo = '1';

/* Vérifier si un adhérent a le droit ou non d’emprunter */
SELECT droit_pret AS droit_pret_Richard
FROM Adherent A
WHERE A.mail = 'richard.duchateau@laposte.fr' AND A.nom = 'Duchateau' AND A.prenom = 'Richard';
/* idem pour un adherent qui n'a plus le droit pour verifier */
SELECT droit_pret
FROM Adherent A
WHERE A.mail = 'paul.russel@gmail.com' AND A.nom = 'Russel' AND A.prenom = 'Paul';

/* Regarder le synopsis d'un film 
SELECT synopsis 
FROM Film 
WHERE Film.code = '3'; */
/* Modifier le synopsis d’un film ( = description d’une ressource) */
UPDATE Film
SET synopsis = 'ceci est un synopsis'
WHERE Film.code = '3';
/* Regarder le synopsis d'un film */
SELECT synopsis 
FROM Film 
WHERE Film.code = '3';

/* Regarder les adhérents avec une ou plusieurs sanctions */
SELECT Adherent.nom AS nom_du_sanctionné, Adherent.prenom AS prenom_du_sanctionné, Adherent.mail AS mail, COUNT(Pret.t_sanction) AS nombre_de_sanction
FROM Adherent 
INNER JOIN Pret ON Adherent.nom = Pret.nom AND Adherent.prenom = Pret.prenom AND Adherent.mail = Pret.mail
WHERE t_sanction IS NOT NULL
GROUP BY Adherent.nom, Adherent.prenom, Adherent.mail;

/* Établir des statistiques sur les documents empruntés par les adhérents :

 Classement des ressources empruntées de la plus empruntée à la moins empruntée */
SELECT COUNT(*) AS Nombre_d_emprunt,R.titre
FROM Ressource R
INNER JOIN Exemplaire E ON E.ressource = R.code
INNER JOIN Pret P ON P.id = E.id
GROUP BY R.code 
ORDER BY COUNT(*) DESC;

/* Classement des ressources empruntées de la plus empruntée à la moins empruntée pour un mois donné */

SELECT COUNT(*) AS Nombre_d_emprunt_mois_de_mai,R.titre
FROM Ressource R
INNER JOIN Exemplaire E ON E.ressource = R.code
INNER JOIN Pret P ON P.id = E.id
WHERE P.date_pret BETWEEN TO_DATE('20210501','YYYYMMDD') AND TO_DATE('20210601','YYYYMMDD')
GROUP BY R.code 
ORDER BY COUNT(*) DESC;

/* Obtenir tous les prêts d’un adhérent */
SELECT Ressource.titre AS Prets_de_Marie, Pret.date_pret, Pret.duree FROM Pret  
INNER JOIN Exemplaire ON Pret.id = Exemplaire.id 
INNER JOIN Ressource ON exemplaire.ressource = ressource.code
WHERE prenom = 'Marie' AND nom = 'Dupon' AND mail = 'marie.dupon@ville-compiegne.fr';

/* Tous les pret actuels d'un adherent */
SELECT Ressource.titre AS Prets_en_cours_de_Marie, Pret.date_pret, Pret.duree FROM Pret  
INNER JOIN Exemplaire ON Pret.id = Exemplaire.id 
INNER JOIN Ressource ON exemplaire.ressource = ressource.code
WHERE prenom = 'Marie' AND nom = 'Dupon' AND mail = 'marie.dupon@ville-compiegne.fr' AND Pret.actuel = '1';

/* Regrouper tous les titres des ressources d’un même éditeur “lambda” */
SELECT titre AS Ressources_éditées_par_MusiqueClassique FROM Ressource WHERE editeur = 'MusiqueClassique';

/* Obtenir le titre des livres en anglais */
SELECT titre AS titres_livres_anglais FROM Livre 
INNER JOIN Ressource ON Livre.code = Ressource.code 
WHERE langue = 'anglais';

/* Obtenir le titre des films de moins de deux heures */

SELECT titre AS titres_de_film_court FROM Film 
JOIN Ressource ON Film.code = Ressource.code 
WHERE Film.longueur <= '02:00:00' ;

/* Obtenir le titre des enregistrements musicaux d’au moins 10 minutes */

SELECT titre AS titres_musique_longs FROM EnregistrementMusical 
JOIN Ressource ON EnregistrementMusical.code = Ressource.code 
WHERE EnregistrementMusical.longueur >= '00:10:00' ;

