import psycopg2
import Suppression
import Afficher
from datetime import date

def ajoutFilm (conn) :
    cur = conn.cursor()
    code = int(input("Entrez le numéro du film : "))
    titre = input("Entrez le titre du film : ")
    titre=titre.lower()
    date_app = input("Entrez la date d’apparition du film : ")
    editeur = input("Entrez l’éditeur du film : ")
    editeur = editeur.lower()
    genre = input("Entrez le genre du film : ")
    genre = genre.lower()
    code_class = int(input("Entrez le code de classification du film : "))
    synopsis = input("Entrez le synopsis du film : ")
    longueur = input("Entrez la longueur du film : ")
    langue = input("Entrez une langue : ")
    langue = langue.lower()
    
    sql = "INSERT INTO Ressource VALUES (%i,'%s',TO_DATE('%s','YYYYMMDD'),'%s','%s',%i);" % (code, titre, date_app, editeur, genre, code_class)
    try:
        cur.execute(sql)
        conn.commit()
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Insertion annulée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Insertion annulée")
        print("Message système :", e)
        conn.rollback()
        return

    sqlbis = "INSERT INTO Film VALUES (%i,'%s','%s','%s');" % (code, synopsis, longueur, langue)
    try:
        cur.execute(sqlbis)
        conn.commit()
    except psycopg2.DataError as e:
        print("\n   Insertion annulée")
        print("Message système :", e)
        conn.rollback()
        return
    except psycopg2.IntegrityError as e:
        print("\n   Insertion annulée")
        print("Message système :", e)
        conn.rollback()
        return

    nb_rea = int(input("\n\nEntrez le nombre de réalisateurs : "))
    for i in range (0,nb_rea):

        while True:
            choice3 = input("\nLe réalisateur n°%i est-il déjà dans la base de données ? (y | n) " % (i+1))
            if(choice3 == 'y' or choice3 == 'n'):
                    break
            else :
                print("    --Veuillez renseigner soit y soit n--")
                continue
    
        if choice3 == 'y' :
            ident = int(input("Entrez l’id du réalisateur n°%i : " % (i+1)))

            try:
                sql2 = "INSERT INTO Realisateur VALUES (%i,%i);" % (ident, code)
                cur.execute(sql2)
                conn.commit()
            except psycopg2.DataError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                Suppression.supprimerFilm(conn, code) #l'insertion du film a été fait mais pas celle des realisateurs ( donc appel ici a la fonction supprimer film )
                return
            except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                Suppression.supprimerFilm(conn, code) #l'insertion du film a été fait mais pas celle des realisateurs ( donc appel ici a la fonction supprimer film )
                return
                
                

        else:
            ident = int(input("Entrez l’id du réalisateur n°%i : " % (i+1)))
            nom = input("Entrez le nom du réalisateur n°%i : " % (i+1))
            nom = nom.lower()
            prenom = input("Entrez le prénom du réalisateur n°%i : " % (i+1))
            prenom = prenom.lower()
            date = input("Entrez la date de naissance du réalisateur n°%i : " % (i+1))
            natio = input("Entrez la nationalité du réalisateur n°%i : " % (i+1))
            natio = natio.lower()

            sql3 = "INSERT INTO Contributeur VALUES (%i,'%s','%s',TO_DATE('%s','YYYYMMDD'),'%s');" % (ident, nom, prenom, date, natio)
            try:
                cur.execute(sql3)
                conn.commit()
            except psycopg2.DataError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                Suppression.supprimerFilm(conn, code)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                Suppression.supprimerFilm(conn, code)
                return
            

            sql4 = "INSERT INTO Realisateur VALUES (%i,%i);" % (ident, code)
            try:
                cur.execute(sql4)
                conn.commit()
            except psycopg2.DataError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                Suppression.supprimerFilm(conn, code)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                Suppression.supprimerFilm(conn, code)
                return
            

    nb_ac = int(input("\n\nEntrez le nombre d’acteurs : "))
    for i in range (nb_ac):

        while True:
            choice3 = input("\nL’acteur n°%i est-il déjà dans la base de données ? (y | n) " % (i+1))
            if(choice3 == 'y' or choice3 == 'n'):
                    break
            else :
                print("    --Veuillez renseigner soit y soit n--")
                continue

        if choice3 == 'y' :
            ident = int(input("Entrez l’id de l’acteur n°%i : " % (i+1)))

            sql2 = "INSERT INTO Acteur VALUES (%i,%i);" % (ident, code)
            try:
                cur.execute(sql2)
                conn.commit()
            except psycopg2.DataError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                Suppression.supprimerFilm(conn, code)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                Suppression.supprimerFilm(conn, code)
                return
            
        else:
            ident = int(input("Entrez l’id de l’acteur n°%i : " % (i+1)))
            nom = input("Entrez le nom de l’acteur n°%i : " % (i+1))
            nom = nom.lower()
            prenom = input("Entrez le prénom de l’acteur n°%i : " % (i+1))
            prenom = prenom.lower()
            date = input("Entrez la date de naissance de l’acteur n°%i : " % (i+1))
            natio = input("Entrez la nationalité de l’acteur n°%i : " % (i+1))
            natio = natio.lower()

            sql3 = "INSERT INTO Contributeur VALUES (%i,'%s','%s',TO_DATE('%s','YYYYMMDD'),'%s');" % (ident, nom, prenom, date, natio)
            try: 
                cur.execute(sql3)
                conn.commit()
            except psycopg2.DataError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                Suppression.supprimerFilm(conn, code)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                Suppression.supprimerFilm(conn, code)
                return
            

            sql4 = "INSERT INTO Acteur VALUES (%i, %i);" % (ident, code)
            try:
                cur.execute(sql4)
                conn.commit()
            except psycopg2.DataError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                Suppression.supprimerFilm(conn, code)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                Suppression.supprimerFilm(conn, code)
                return

    print("\n   Insertion du Film reussi")
            
def ajoutLivre (conn):
    cur = conn.cursor()
    code = int(input("Entrez le code du livre: "))
    titre = input("Entrez le titre du livre : ")
    titre = titre.lower()
    date_app = input("Entrez la date d’apparition du livre : ")
    editeur = input("Entrez l’éditeur du livre: ")
    editeur = editeur.lower()
    genre = input("Entrez le genre du livre : ")
    genre = genre.lower()
    code_class = int(input("Entrez le code de classification du livre : "))
    isbn = int(input("Entrez le numéro isbn du livre: "))
    resume = input("Entrez le résumé du livre: ")
    langue = input ("Entrez la langue du livre: ")
    langue = langue.lower()

    try:
        sql = "INSERT INTO Ressource VALUES (%i,'%s',TO_DATE('%s','YYYYMMDD'),'%s','%s',%i);" % (code, titre, date_app, editeur, genre, code_class)
        cur.execute(sql)
        conn.commit()
    except psycopg2.DataError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                return
    except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                return

    try:
        sqlbis = "INSERT INTO Livre VALUES (%i,%i,'%s','%s');" %(code, isbn ,resume, langue)
        cur.execute(sqlbis)
        conn.commit()
    except psycopg2.DataError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                Suppression.supprimerLivre(conn, code)
                return
    except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                Suppression.supprimerLivre(conn, code)
                return

    nb_aut = int(input("Entrez le nombre d’auteurs : "))
    for i in range (nb_aut):
        while True:
            choice3 = input("L’auteur n°%i est-il déjà dans la base de données ? (y | n) " % (i+1))
            if(choice3 == 'y' or choice3 == 'n'):
                    break
            else :
                print("    --Veuillez renseigner soit y soit n--")
                continue

        if choice3 == 'y' :
            ident = int(input("Entrez l’id de l’auteur n°%i : " % (i+1)))

            try:
                sql2 = "INSERT INTO ecrivain VALUES (%i,%i);" % (ident, code)
                cur.execute(sql2)
                conn.commit()
                Suppression.supprimerLivre(conn, code)
            except psycopg2.DataError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                Suppression.supprimerLivre(conn, code)
                conn.rollback()
                return
            except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                Suppression.supprimerLivre(conn, code)
                return
                #supprimer le livre ensuite car l'insertion du livre a été fait mais pas celle des auteurs (faire appel ici a la fonction supprimer livre)

        else:
            ident = int(input("Entrez l’id de l’auteur n°%i : " % (i+1)))
            nom = input("Entrez le nom de l’auteur n°%i : " % (i+1))
            nom = nom.lower()
            prenom = input("Entrez le prénom de l’auteur n°%i : " % (i+1))
            prenom = prenom .lower()
            date = input("Entrez la date de naissance de l’auteur n°%i : " % (i+1))
            natio = input("Entrez la nationalité de l’auteur n°%i : " % (i+1))
            natio = natio.lower()

            try:
                sql3 = "INSERT INTO Contributeur VALUES (%i,'%s','%s',TO_DATE('%s','YYYYMMDD'),'%s');" % (ident, nom, prenom, date, natio)
                cur.execute(sql3)
                conn.commit()
            except psycopg2.DataError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                Suppression.supprimerLivre(conn, code)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                Suppression.supprimerLivre(conn, code)
                return

            try:
                sql4 = "INSERT INTO ecrivain VALUES (%i,%i);" % (ident, code)
                cur.execute(sql4)
                conn.commit()
            except psycopg2.DataError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                Suppression.supprimerLivre(conn, code)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                Suppression.supprimerLivre(conn, code)
                return
    print("\n   Insertion du livre réussi")
            


def ajoutMembre(conn):
    cur = conn.cursor()
    mail = input("Entrez le mail du membre : ")
    nom = input("Entrez le nom du membre : ")
    nom = nom.lower()
    prenom = input("Entrez le prénom du membre : ")
    prenom = prenom.lower()
    date = input("Entrez la date de naissance du membre : ")
    adresse = input("Entrez l’adresse du membre : ")
    login = input("Entrez le login du compte associé : ")
    mdp = input("Entrez le mot de passe du compte associé : ")

    sql = "INSERT INTO Utilisateur VALUES ('%s','%s','%s',TO_DATE('%s','YYYYMMDD'),'%s');" % (mail, nom, prenom, date, adresse)
    try:
        cur.execute(sql)
        conn.commit()
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Insertion annulée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Insertion annulée")
        print("Message système :", e)
        conn.rollback()
        return

    sql1 = "INSERT INTO Membre VALUES ('%s','%s','%s');" % (mail, nom, prenom)
    try:
        cur.execute(sql1)
        conn.commit()
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Insertion annulée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Insertion annulée")
        print("Message système :", e)
        conn.rollback()
        return
    
    try:   
        sql2 = "INSERT INTO CompteUtilisateur VALUES ('%s','%s','%s','%s','%s');" % (login, mdp, mail, nom, prenom)
        cur.execute(sql2)
        conn.commit()
        print("\n   Insertion réussi")
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Insertion annulée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Insertion annulée")
        print("Message système :", e)
        conn.rollback()
        return

def ajoutAdherent(conn):
    cur = conn.cursor()
    mail = input("Entrez le mail de l’adhérent : ")
    nom = input("Entrez le nom de l’adhérent : ")
    nom = nom.lower()
    prenom = input("Entrez le prénom de l’adhérent : ")
    prenom = prenom.lower()
    date = input("Entrez la date de naissance de l’adhérent : ")
    adresse = input("Entrez l’adresse de l’adhérent : ")
    telephone = int(input("Entrez le numéro de téléphone de l’adhérent : "))
    login = input("Entrez le login du compte associé : ")
    mdp = input("Entrez le mot de passe du compte associé : ")
    
    sql = "INSERT INTO Utilisateur VALUES ('%s','%s','%s',TO_DATE('%s','YYYYMMDD'),'%s');" % (mail, nom, prenom, date, adresse)
    try:
        cur.execute(sql)
        conn.commit()
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Insertion annulée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Insertion annulée")
        print("Message système :", e)
        conn.rollback()
        return

    sql1 = "INSERT INTO Adherent VALUES ('%s','%s','%s','1', NULL,'1',%i);" % (mail, nom, prenom, telephone)
    try:
        cur.execute(sql1)
        conn.commit()
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Insertion annulée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Insertion annulée")
        print("Message système :", e)
        conn.rollback()
        return

    try:   
        sql2 = "INSERT INTO CompteUtilisateur VALUES ('%s','%s','%s','%s','%s');" % (login, mdp, mail, nom, prenom)
        cur.execute(sql2)
        conn.commit()
        print("\n   Insertion réussi")
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Insertion annulée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Insertion annulée")
        print("Message système :", e)
        conn.rollback()
        return

def ajoutEnregistrement(conn):
    cur = conn.cursor()
    code = int(input("Entrez le numéro de l’enregistrement musical : "))
    titre = input("Entrez le titre de l’enregistrement musical : ")
    titre = titre.lower()
    date_app = input("Entrez la date d’apparition de l’enregistrement musical : ")
    editeur = input("Entrez l’éditeur de l’enregistrement musical : ")
    editeur = editeur.lower()
    genre = input("Entrez le genre de l’enregistrement musical : ")
    genre = genre.lower()
    code_class = int(input("Entrez le code de classification de l’enregistrement musical : "))
    longueur = input("Entrez la longueur de l’enregistrement : ")

    try:
        sql = "INSERT INTO Ressource VALUES (%i,'%s', TO_DATE('%s','YYYYMMDD'),'%s','%s',%i);" % (code, titre, date_app, editeur, genre, code_class)
        cur.execute(sql)
        conn.commit()
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Insertion annulée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Insertion annulée")
        print("Message système :", e)
        conn.rollback()
        return
    
    try:
        sqlbis = "INSERT INTO Enregistrementmusical VALUES (%i,'%s');" % (code, longueur)
        cur.execute(sqlbis)
        conn.commit()
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Insertion annulée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Insertion annulée")
        print("Message système :", e)
        conn.rollback()
        return

    nb_interprete = int(input("Entrez le nombre d’interpretes : "))

    for i in range (nb_interprete):
        while True:
            choice3 = input("L’interprète n°%i est-il déjà dans la base de données ? (y | n) " % (i+1))
            if(choice3 == 'y' or choice3 == 'n'):
                    break
            else :
                print("    --Veuillez renseigner soit y soit n--")
                continue
        if choice3 == 'y' :
            ident = int(input("Entrez l’id de l'interprète n°%i : " % (i+1)))

            try: 
                sql2 = "INSERT INTO Interprete VALUES (%i,%i);" % (ident, code)
                cur.execute(sql2)
                conn.commit()
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Insertion annulée")
                print("Message système :", e)
                Suppression.supprimerEnregistrement(conn, code)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                Suppression.supprimerEnregistrement(conn, code)
                return

        else:
            ident = int(input("Entrez l’id de l'interprète n°%i : " % (i+1)))
            nom = input("Entrez le nom de l'interprète n°%i : " % (i+1))
            nom = nom.lower()
            prenom = input("Entrez le prénom de l'interprète n°%i : " % (i+1))
            prenom = prenom.lower()
            date = input("Entrez la date de naissance de l'interprète n°%i : " % (i+1))
            natio = input("Entrez la nationalité de l'interprète n°%i : " % (i+1))
            natio = natio.lower()

            try: 
                sql3 = "INSERT INTO Contributeur VALUES (%i,'%s','%s',TO_DATE('%s','YYYYMMDD'),'%s');" % (ident, nom, prenom, date, natio)
                cur.execute(sql3)
                conn.commit()
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Insertion annulée")
                print("Message système :", e)
                Suppression.supprimerEnregistrement(conn, code)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                Suppression.supprimerEnregistrement(conn, code)
                return

            try:
                sql4 = "INSERT INTO Interprete VALUES (%i,%i);" % (ident, code)
                cur.execute(sql4)
                conn.commit()
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Insertion annulée")
                print("Message système :", e)
                Suppression.supprimerEnregistrement(conn, code)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                Suppression.supprimerEnregistrement(conn, code)
                return

    nb_compo = int(input("Entrez le nombre de compositeurs : "))
    for i in range (nb_compo):
        
        while True:
            choice3 = input("Le compositeur n°%i est-il déjà dans la base de données ? (y | n) " % (i+1))
            if(choice3 == 'y' or choice3 == 'n'):
                    break
            else :
                print("    --Veuillez renseigner soit y soit n--")
                continue

        if choice3 == 'y' :
            ident = int(input("Entrez l’id du compositeur n°%i : " % (i+1)))

            try : 
                sql2 = "INSERT INTO Compositeur VALUES (%i,%i);" % (ident, code)
                cur.execute(sql2)
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Insertion annulée")
                print("Message système :", e)
                Suppression.supprimerEnregistrement(conn, code)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                Suppression.supprimerEnregistrement(conn, code)
                conn.rollback()
                return
        else:
            ident = int(input("Entrez l’id du compositeur n°%i : " % (i+1)))
            nom = input("Entrez le nom du compositeur n°%i : " % (i+1))
            nom = nom.lower()
            prenom = input("Entrez le prénom du compositeur n°%i : " % (i+1))
            prenom = prenom.lower()
            date = input("Entrez la date de naissance du compositeur n°%i : " % (i+1))
            natio = input("Entrez la nationalité du compositeur n°%i : " % (i+1))
            natio = natio.lower()
            
            try:
                sql3 = "INSERT INTO Contributeur VALUES (%i,'%s','%s','%s','%s');" % (ident, nom, prenom, date, natio)
                cur.execute(sql3)
                conn.commit()
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Insertion annulée")
                print("Message système :", e)
                Suppression.supprimerEnregistrement(conn, code)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                Suppression.supprimerEnregistrement(conn, code)
                return

            try:
                sql4 = "INSERT INTO Compositeur VALUES (%i, %i);" % (ident, code)
                cur.execute(sql4)
                conn.commit()
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Insertion annulée")
                print("Message système :", e)
                Suppression.supprimerEnregistrement(conn, code)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                Suppression.supprimerEnregistrement(conn, code)
                return
    print("\n   Insertion de l'enregistrement réussi")

def ajoutPret(conn):
    cur = conn.cursor()
    mail = input("Entrez le mail de l’adhérent souhaitant emprunter un exemplaire : ")
    nom = input("Entrez le nom de l’adhérent souhaitant emprunter un exemplaire : ")
    nom = nom.lower()
    prenom = input("Entrez le prénom de l’adhérent souhaitant emprunter un exemplaire : ")
    prenom = prenom.lower()
    try:
        sql = "SELECT * FROM adherent where mail = '%s' AND nom = '%s' AND prenom = '%s' AND droit_pret='%s' AND adhesion='%s' ;" % ( mail, nom, prenom,'t','t')
        cur.execute(sql)        
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Insertion annulée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Insertion annulée")
        print("Message système :", e)
        conn.rollback()
        return
    raw = cur.fetchone()

    if raw :
        print("\n- L'adherent peut d'emprunter\n")
        ident = int(input("Entrez l’identifiant de l’exemplaire à emprunter : "))
        try:
            sql = "SELECT * FROM exemplaire where id = '%s' AND (etat='%s' or etat='%s') and dispo='%s' ;" % (ident,'bon','neuf','t')
            cur.execute(sql)        
        except psycopg2.DataError as e:
            conn.rollback()
            print("\n   Insertion annulée")
            print("Message système :", e)
            return
        except psycopg2.IntegrityError as e:
            print("\n   Insertion annulée")
            print("Message système :", e)
            conn.rollback()
            return
        raw = cur.fetchone()
        if raw:
            print("\n- L'exemplaire peut être emprunté")

            print("\n- Un adherent peut emprunter au maximum 4 livres, 3 enregistrements, 3 films")
            print("Analyse du nombre d'emprunt par type pour l'adhèrent : ")

            #Recherche du type de la ressource Pour verifier que l'adherent n'a pas atteint le maximum d'emprunt par type de ressource
            #Test si c'est un livre
            try:
                sql = "SELECT * FROM Exemplaire E, Ressource R, Livre L WHERE E.id=%i AND E.ressource=R.code AND R.code=L.code" %(ident)
                cur.execute(sql)
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Insertion annulée")
                print("Message système :", e)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                return
            rawLivre1 = cur.fetchone()

            #Comptage du nombre de livres actuellement empruntés
            try:
                sql2 = "SELECT COUNT(*) FROM Pret P, Exemplaire E, Ressource R, Livre L WHERE mail='%s' AND nom='%s' AND prenom='%s' AND actuel='%s' AND P.id=E.id AND E.ressource=R.code AND R.code=L.code" %(mail, nom, prenom,'t')
                cur.execute(sql2)
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Insertion annulée")
                print("Message système :", e)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                return
            rawLivre = cur.fetchone()
            #Si la demande d'emprunt est sur un livre
            if (rawLivre1 and rawLivre[0]>=4):
                print("Impossible d'effectuer le prêt : trop de livres empruntés")
                return




            #Test si c'est un film
            try:
                sql = "SELECT * FROM Exemplaire E, Ressource R, Film F WHERE E.id=%i AND E.ressource=R.code AND R.code=F.code" %(ident)
                cur.execute(sql)
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Insertion annulée")
                print("Message système :", e)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                return
            rawFilm1= cur.fetchone()

            #Comptage du nombre de films actuellement empruntés
            try:
                sql = "SELECT COUNT(*) FROM Pret P, Exemplaire E, Ressource R, Film F WHERE mail='%s' AND nom='%s' AND prenom='%s' AND P.actuel='%s' AND P.id=E.id AND E.ressource=R.code AND R.code=F.code" %(mail, nom, prenom,'t')
                cur.execute(sql)
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Insertion annulée")
                print("Message système :", e)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                return
            rawFilm = cur.fetchone()
            #Si la demande d'emprunt est sur un film
            if (rawFilm1 and rawFilm[0]>=3):
                print("Impossible d'effectuer le prêt : trop de films empruntés")
                return




            #Test si c'est un enregistrement musical
            try:
                sql = "SELECT * FROM Exemplaire E, Ressource R, Film F, EnregistrementMusical M WHERE E.id=%i AND E.ressource=R.code AND R.code=M.code" %(ident)
                cur.execute(sql)
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Insertion annulée")
                print("Message système :", e)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                return
            rawEnr1 = cur.fetchone()

            #Comptage du nombre d'enregistrement actuellement empruntés
            try:
                sql = "SELECT COUNT(*) FROM Pret P, Exemplaire E, Ressource R, EnregistrementMusical M WHERE mail='%s' AND nom='%s' AND prenom='%s' AND P.actuel='%s' AND P.id=E.id AND E.ressource=R.code AND R.code=M.code" %(mail, nom, prenom,'t')
                cur.execute(sql)
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Insertion annulée")
                print("Message système :", e)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                return
            rawEnr = cur.fetchone()
            #Si la demande d'emprunt est sur un enr
            if (rawEnr1 and rawEnr[0]>=3) :
                print("Impossible d'effectuer le prêt : trop d'enregistrements musicaux empruntés")
                return

            print("Emprunt possible car l'adherent a actuellement un emprunt de '%s' film(s), de '%s' livre(s), et de '%s' enregistrement(s)\n" %(rawFilm[0],rawLivre[0],rawEnr[0]))
            aujourdhui = date.today()
            while True:
                    duree = int(input("Entrez la durée maximale du prêt (en nombre de jour <30): ")) 
                    if(0<duree<30):
                            break
                    else :
                        print("    --Erreur veuillez rentrer un entier entre 0 et 30--")
                        continue
            
            try:
                sql = "INSERT INTO Pret VALUES (%i,'%s','%s','%s','%s','%s', NULL, NULL, NULL, NULL,'1');" % (ident, aujourdhui, mail, nom, prenom, duree)
                cur.execute(sql)
                conn.commit()
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Insertion annulée")
                print("Message système :", e)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                return
            try:
                sql2 = "UPDATE exemplaire Set dispo ='%s' where id='%s';" % ('f',ident)
                cur.execute(sql2)
                conn.commit()
                print("\n   Insertion réussi")
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Insertion annulée")
                print("Message système :", e)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Insertion annulée")
                print("Message système :", e)
                conn.rollback()
                return
        else:
            print("\nL'exemplaire n'est pas disponible ou n'est pas dans un etat suffisant")
        
    else : 
         print("\nL'adherent ne peut pas emprunter")
         return

def ajoutExemplaire(conn):
    cur = conn.cursor()
    ressource = int(input("Entrez le code de la ressource associée à l'exemplaire : "))
    ident = int(input("Entrez l'identifiant de l'exemplaire : "))
    prix = float(input("Entrez le prix de l'exemplaire : "))
    etat = input("Entrez l'état de l'exemplaire (neuf|bon|abime|perdu) : ")

    sql = "INSERT INTO Exemplaire VALUES (%i,%f,'%s','t',%i);" % (ident, prix, etat, ressource)
    try:
        cur.execute(sql)
        conn.commit()
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Insertion annulée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Insertion annulée")
        print("Message système :", e)
        conn.rollback()
        return
