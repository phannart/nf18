import psycopg2
def AfficherRessource(conn):
    cur = conn.cursor()
    sql1 = "SELECT * FROM ressource ORDER BY code;"
    cur.execute(sql1)
    print("\n[Code] c:  Titre  :  Date d’apparition  :  Editeur  :  Genre  :  Code classification  ")
    raw = cur.fetchone()
    while raw:
        print("[%s] : %s : %s : %s : %s : %s " %(raw[0],raw[1],raw[2], raw[3], raw[4], raw[5]))
        raw = cur.fetchone()

def afficherLivre(conn):
    cur = conn.cursor()
    sql = "SELECT ressource.code, titre, date_apparition,editeur, genre ,code_classification, isbn, resum, langue FROM Ressource INNER JOIN Livre ON Ressource.code=Livre.code ORDER BY code;"
    cur.execute(sql)
    print("\n[Code]  :  Titre  :  Date d’apparition  :  Editeur  :  Genre  : Code classification  :  isbn  :  resum   :  language")
    raw = cur.fetchone()
    while raw:
        print("[%s] : %s : %s : %s : %s : %s : %s : '%s' : '%s' " %(raw[0],raw[1],raw[2], raw[3], raw[4], raw[5], raw [6], raw[7], raw [8]))
        raw = cur.fetchone()
    print("\n ---Les Ecrivains :---")
    sql3 = "SELECT ressource.code, ressource.titre, contributeur.id, contributeur.nom, contributeur.prenom FROM Ressource INNER JOIN livre ON Ressource.code=livre.code INNER JOIN Ecrivain ON Ecrivain.code=livre.code INNER JOIN Contributeur ON contributeur.id=Ecrivain.id ORDER BY ressource.code;"
    cur.execute(sql3)
    print("\n Code_Ressource : Titre : Id_Ecrivain  : Nom : Prenom ")
    raw = cur.fetchone()
    while raw:
        print("[%s]  : %s  : %s  : %s  : %s  " %(raw[0],raw[1],raw[2], raw[3], raw[4]))
        raw = cur.fetchone()

def afficherEnregistrement(conn):
    cur = conn.cursor()
    sql = "SELECT ressource.code, titre, date_apparition,editeur, genre ,code_classification, longueur FROM Ressource INNER JOIN EnregistrementMusical ON Ressource.code=EnregistrementMusical.code ORDER BY ressource.code;"
    cur.execute(sql)
    print("\n[Code]  :  Titre  :  Date d’apparition  :  Editeur  :  Genre    :  Code classification  :  longueur")
    raw = cur.fetchone()
    while raw:
        print("[%s] : %s : %s : %s : %s : %s : '%s'" %(raw[0],raw[1],raw[2], raw[3], raw[4], raw[5], raw[6]))
        raw = cur.fetchone()

    print("\n ---Les Interpretes :---")
    sql2 = "SELECT ressource.code, ressource.titre, contributeur.id, contributeur.nom, contributeur.prenom FROM Ressource INNER JOIN EnregistrementMusical ON Ressource.code=EnregistrementMusical.code INNER JOIN Interprete ON Interprete.code=EnregistrementMusical.code INNER JOIN Contributeur ON contributeur.id=Interprete.id ORDER BY ressource.code;"
    cur.execute(sql2)
    print("\n Code_Ressource : Titre : Id_Interprete : Nom : Prenom ")
    raw = cur.fetchone()
    while raw:
        print("[%s]  : %s  : %s  : %s  : %s  " %(raw[0],raw[1],raw[2], raw[3], raw[4]))
        raw = cur.fetchone()

    print("\n ---Les Compositeurs :---")
    sql3 = "SELECT ressource.code, ressource.titre, contributeur.id, contributeur.nom, contributeur.prenom FROM Ressource INNER JOIN EnregistrementMusical ON Ressource.code=EnregistrementMusical.code INNER JOIN Compositeur  ON Compositeur.code=EnregistrementMusical.code INNER JOIN Contributeur ON contributeur.id=Compositeur.id ORDER BY ressource.code;"
    cur.execute(sql3)
    print("\n Code_Ressource : Titre : Id_Compositeur  : Nom : Prenom ")
    raw = cur.fetchone()
    while raw:
        print("[%s]  : %s  : %s  : %s  : %s  " %(raw[0],raw[1],raw[2], raw[3], raw[4]))
        raw = cur.fetchone()

def afficherFilm(conn):
    cur = conn.cursor()
    sql = "SELECT ressource.code, titre, date_apparition,editeur,genre ,code_classification, synopsis, longueur,langue FROM Ressource INNER JOIN Film ON Ressource.code=Film.code ORDER BY code;"
    cur.execute(sql)
    print("\n[Code]  :  Titre  :  Date d’apparition  :  Editeur  :  Genre  :  Code classification  :  synopsis  :  longueur  :  langue")
    raw = cur.fetchone()
    while raw:
        print("[%s] : %s : %s : %s : %s : %s : '%s' : '%s': '%s' " %(raw[0],raw[1],raw[2], raw[3], raw[4], raw[5], raw [6], raw[7], raw [8]))
        raw = cur.fetchone()

    print("\n ---Les realisateurs :---")
    sql2 = "SELECT ressource.code, ressource.titre, contributeur.id, contributeur.nom, contributeur.prenom FROM Ressource INNER JOIN Film ON Ressource.code=Film.code INNER JOIN Realisateur ON realisateur.code=Film.code INNER JOIN Contributeur ON contributeur.id=realisateur.id ORDER BY ressource.code;"
    cur.execute(sql2)
    print("\n Code_Ressource : Titre : Id_realisateur : Nom : Prenom ")
    raw = cur.fetchone()
    while raw:
        print("[%s]  : %s  : %s  : %s  : %s  " %(raw[0],raw[1],raw[2], raw[3], raw[4]))
        raw = cur.fetchone()
    print("\n ---Les acteurs :---")
    sql3= "SELECT ressource.code, ressource.titre, contributeur.id, contributeur.nom, contributeur.prenom FROM Ressource INNER JOIN Film ON Ressource.code=Film.code INNER JOIN Acteur ON acteur.code=Film.code INNER JOIN Contributeur ON contributeur.id=acteur.id ORDER BY ressource.code;"
    cur.execute(sql3)
    print("\n Code_Ressource  :  Titre  :  Id_acteur  :  Nom  :  Prenom ")
    raw = cur.fetchone()
    while raw:
        print("[%s] : %s : %s : %s : %s  " %(raw[0],raw[1],raw[2], raw[3], raw[4]))
        raw = cur.fetchone()

def afficherDispo(conn):
    cur = conn.cursor()
    ident = int(input("Entrez l'identifiant de la ressource : "))
    sql = "SELECT id FROM Exemplaire WHERE Exemplaire.ressource = %i AND Exemplaire.dispo = '1';" %(ident)
    cur.execute(sql)
    raw = cur.fetchone()
    if raw:
        print("\nIdentifiant des exemplaires disponibles : ")
        while raw:
            print("%s  " %(raw[0]))
            raw = cur.fetchone()
    else :
        print("Aucun exemplaires disponibles")

def stat(conn):
    cur = conn.cursor()
    sql = "SELECT COUNT(*),R.titre FROM Ressource R INNER JOIN Exemplaire E ON E.ressource = R.code INNER JOIN Pret P ON P.id = E.id GROUP BY R.code  ORDER BY COUNT(*) DESC;"
    cur.execute(sql)
    raw = cur.fetchone()
    print("\nNombre d'emprunt  :  titre ")
    while raw:
        print("%s                 :  %s  " %(raw[0],raw[1]))
        raw = cur.fetchone()

def statperiode(conn):
    cur = conn.cursor()
    datedebut = input("Entrez la date de debut de la periode : ")
    datefin = input("Entrez la date de fin de la periode : ")
    sql = "SELECT COUNT(*),R.titre FROM Ressource R INNER JOIN Exemplaire E ON E.ressource = R.code INNER JOIN Pret P ON P.id = E.id WHERE P.date_pret BETWEEN TO_DATE('%s','YYYYMMDD') AND TO_DATE('%s','YYYYMMDD') GROUP BY R.code  ORDER BY COUNT(*) DESC;"%(datedebut,datefin)
    cur.execute(sql)
    raw = cur.fetchone()
    print("\nNombre d'emprunt :  titre ")
    while raw:
        print("%s                 :  %s  " %(raw[0],raw[1]))
        raw = cur.fetchone()

def AfficherAdherent(conn):
    cur = conn.cursor()
    sql1 = "SELECT * FROM Adherent ORDER BY nom;"
    cur.execute(sql1)
    print("\nNom  :  Prenom  :  Mail  :  Telephone  :  Droit_Pret  :  Date_av_nouvel_emprunt  :  Adhesion ")
    raw = cur.fetchone()
    while raw:
        print("%s : %s : %s : %s : %s : %s : %s" %(raw[1],raw[2],raw[0], raw[6], raw[3], raw[4], raw[5]))
        raw = cur.fetchone()

def AfficherMembre(conn): 
    cur = conn.cursor()
    sql1 = "SELECT M.mail, M.nom, M.prenom, adresse FROM Membre M JOIN Utilisateur U ON M.mail=U.mail AND M.nom=U.nom AND M.prenom=U.prenom ORDER BY nom;"
    cur.execute(sql1)
    print("\nNom  :  Prenom  :  Mail : Adresse ")
    raw = cur.fetchone()
    while raw:
        print("%s : %s : %s : %s" %(raw[1],raw[2],raw[0],raw[3]))
        raw = cur.fetchone()


def AfficherPretMembre(conn):
    cur = conn.cursor()
    nom = input("Entrez le nom de l'adhérent : ")
    nom = nom.lower()
    prenom = input("Entrez le prénom de l'adhérent : ")
    prenom = prenom.lower()
    mail = input("Entrez le mail de l'adhérent : ")
    sql = "SELECT date_pret, id, duree, actuel ,t_sanction ETAT, a_payer FROM Pret WHERE mail = '%s' AND nom = '%s' AND prenom = '%s'" %(mail, nom, prenom)
    sanction = 0
    try:
        cur.execute(sql)
        raw = cur.fetchone()
        if raw : 
            print("\nLes prets :")
            print("\nDate du pret : Id_Exemplaire : Duree : Actuel?")
            while raw:
                print("%s  :  %s  :  %s jours  :  %s " %(raw[0],raw[1],raw[2],raw[3]))
                if(raw[4]):
                    sanction = 1
                raw = cur.fetchone()
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Affichage annulé")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Affichage annulé")
        print("Message système :", e)
        conn.rollback()
        return
    if sanction == 1:
        print("\nLa(les) sanction(s) associée(s) : ")
        sql2 = "SELECT date_pret, id , t_sanction, nb_jours, nv_etat ETAT, a_payer FROM Pret WHERE mail = '%s' AND nom = '%s' AND prenom = '%s' AND (t_sanction = '%s'  or t_sanction='%s' or t_sanction='%s');" %(mail, nom, prenom,'retard','endommagement','lesdeux')
        try:
            cur.execute(sql2)
            raw = cur.fetchone()
            print("\nDate du pret  :  Id_Exemplaire  :  Sanction  :  Nb_jours_retard : Nouvel_etat : Amende")
            while raw:
                print("%s  :  %s  :  %s  :  %s jours  :  %s  :  %s €" %(raw[0],raw[1],raw[2],raw[3],raw[4],raw[5]))
                raw = cur.fetchone()
        except psycopg2.DataError as e:
            conn.rollback()
            print("\n   Affichage annulé")
            print("Message système :", e)
            return
        except psycopg2.IntegrityError as e:
            print("\n   Affichage annulé")
            print("Message système :", e)
            conn.rollback()
            return
    else:
        print("\n Aucune sanction n'est liée à cet adhèrent \n")
