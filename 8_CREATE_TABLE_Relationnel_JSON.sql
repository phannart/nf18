
DROP TABLE IF EXISTS Membre, Interprete, Compositeur, EnregistrementMusical, Realisateur, Acteur, Film, Ecrivain, Contributeur, Livre, Isbn, Pret, Exemplaire,  Utilisateur, Adherent, Ressource,  CodeClassification;
DROP TYPE IF EXISTS T_SANCTION, ETAT;


CREATE TYPE ETAT AS ENUM ('neuf', 'bon', 'abime', 'perdu');
CREATE TYPE T_SANCTION AS ENUM ('retard', 'endommagement', 'lesdeux');

CREATE TABLE CodeClassification (
   code_classification INTEGER PRIMARY KEY,
   titre VARCHAR NOT NULL,
   date_apparition DATE,
   editeur VARCHAR,
   genre VARCHAR,
   CHECK (code_classification>0 and code_classification<1000)
);

CREATE TABLE Ressource (
   code INTEGER UNIQUE,
   code_classification INTEGER UNIQUE REFERENCES CodeClassification (code_classification),
   Primary key (code,code_classification) 
);


CREATE TABLE Utilisateur (
	mail VARCHAR(60),
	nom VARCHAR(15),
	prenom VARCHAR(15),
    date_naissance DATE,
    adresse JSON NOT NULL,
    compteUtilisateur JSON NOT NULL,
    PRIMARY KEY (mail,nom,prenom)
);

CREATE TABLE Adherent (
	mail VARCHAR(60),
	nom VARCHAR(15),
	prenom VARCHAR(15),
	droit_pret BOOLEAN NOT NULL,
	date_avant_nouvel_emprunt DATE,
	adhesion BOOLEAN NOT NULL,
    telephone INTEGER,
    FOREIGN KEY (mail,nom,prenom) REFERENCES Utilisateur (mail,nom,prenom),
    PRIMARY KEY (mail,nom,prenom)
);

CREATE TABLE Exemplaire (
	id INTEGER PRIMARY KEY,
	prix FLOAT NOT NULL,
    etat ETAT NOT NULL,
	dispo BOOLEAN NOT NULL,
	ressource INTEGER NOT NULL,
	CHECK (prix>1 and prix<100),
	FOREIGN KEY (ressource) REFERENCES Ressource (code)
);

CREATE TABLE Pret( 
    id INTEGER REFERENCES Exemplaire (id),
    date_pret date,
    mail VARCHAR(60),
    nom VARCHAR(15),
    prenom VARCHAR(15),
    duree INTEGER NOT NULL CHECK (duree < 30 AND duree > 0),
	t_sanction JSON,
	actuel BOOLEAN,
    FOREIGN KEY (mail,nom,prenom) REFERENCES Adherent (mail,nom,prenom),
    PRIMARY KEY (id,date_pret,mail,nom,prenom)
);




CREATE TABLE Isbn(
   isbn INTEGER PRIMARY KEY,
   resum TEXT,
   langue VARCHAR
);

CREATE TABLE Livre (
	code INTEGER UNIQUE REFERENCES Ressource (code),
	isbn INTEGER UNIQUE REFERENCES Isbn(isbn),
	PRIMARY KEY (code,isbn)
);



CREATE TABLE Contributeur (
	id INTEGER PRIMARY KEY,
	nom VARCHAR(15) NOT NULL,
	prenom VARCHAR(15) NOT NULL,
	date_naissance DATE,
    nationalite VARCHAR(15)
);


CREATE TABLE Ecrivain (
	id INTEGER,
	code INTEGER,
    FOREIGN KEY (id) REFERENCES Contributeur (id),
    FOREIGN KEY (code) REFERENCES Livre (code),
    PRIMARY KEY(id,code)
);

CREATE TABLE Film (
	code INTEGER REFERENCES Ressource (code) PRIMARY KEY,
	synopsis TEXT,
	longueur TIME,
	langue VARCHAR(15)
);

CREATE TABLE Acteur (
	id INTEGER,
	code INTEGER,
    FOREIGN KEY (id) REFERENCES Contributeur (id),
    FOREIGN KEY (code) REFERENCES Film (code),
    PRIMARY KEY (id,code)
);

CREATE TABLE Realisateur (
	id INTEGER,
	code INTEGER,
    FOREIGN KEY (id) REFERENCES Contributeur (id),
    FOREIGN KEY (code) REFERENCES Film (code),
    PRIMARY KEY (id,code)
);

CREATE TABLE EnregistrementMusical (
	code INTEGER REFERENCES Ressource (code) PRIMARY KEY,
	longueur TIME
);

CREATE TABLE Compositeur (
    id INTEGER REFERENCES Contributeur (id),
    code INTEGER REFERENCES EnregistrementMusical (code),
    PRIMARY KEY (id,code)
);

CREATE TABLE Interprete (
    id INTEGER REFERENCES Contributeur (id),
    code INTEGER REFERENCES EnregistrementMusical (code),
    PRIMARY KEY (id,code)
);


CREATE TABLE Membre (
	mail VARCHAR(60),
    nom VARCHAR(15),
    prenom VARCHAR(15),
    FOREIGN KEY (mail,nom,prenom) REFERENCES Utilisateur (mail,nom,prenom),
    PRIMARY KEY (mail,nom,prenom)
);


/*CREATE TABLE CompteUtilisateur (
	lgin VARCHAR(15),
	mdp VARCHAR(100) NOT NULL,
	mail VARCHAR(60),
	nom VARCHAR(15),
	prenom VARCHAR(15),
    FOREIGN KEY (mail,nom, prenom) REFERENCES Utilisateur (mail,nom,prenom),
	PRIMARY KEY (lgin, mail, nom, prenom)
);*/





