import psycopg2
import Afficher
from datetime import timedelta, date

def cloturePret(conn):
    cur = conn.cursor()
    ident = int(input("Entrez l’identifiant de l’exemplaire emprunté : "))
    datep = input("Entrez la date à laquelle a été effectuée le pret: ")
    mail = input("Entrez le mail de l’adhérent ayant emprunté l’exemplaire : ")
    nom = input("Entrez le nom de l’adhérent ayant emprunté l’exemplaire : ")
    nom = nom.lower()
    prenom = input("Entrez le prénom de l’adhérent ayant emprunté l’exemplaire : ")
    prenom = prenom.lower()
    sql = "Update Pret Set actuel = 'f' Where Pret.id=%i And Pret.date_pret=TO_DATE('%s','YYYYMMDD') And Pret.nom='%s' And Pret.prenom='%s' And Pret.mail='%s';"% (ident, datep, nom, prenom, mail)
    try:
        cur.execute(sql)
        conn.commit()
        print("\n   Modification réussi")
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Modification annulée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Modification annulée")
        print("Message système :", e)
        conn.rollback()
        return
    try:
        sql2 = "UPDATE exemplaire Set dispo ='%s' where id='%s';" % ('t',ident)
        cur.execute(sql2)
        conn.commit()
        print("\n   Modification réussi")
    except psycopg2.DataError as e:
        conn.rollback()
        print("\n   Modification annulée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        print("\n   Modification annulée")
        print("Message système :", e)
        conn.rollback()
        return

def Sanction(conn):
    cur = conn.cursor()
    ident = int(input("Entrez l’identifiant de l’exemplaire lié à la sanction : "))
    datep = input("Entrez la date à laquelle a été effectuée le pret: ")
    mail = input("Entrez le mail de l’adhérent ayant emprunté l’exemplaire : ")
    nom = input("Entrez le nom de l’adhérent ayant emprunté l’exemplaire : ")
    nom = nom.lower()
    prenom = input("Entrez le prénom de l’adhérent ayant emprunté l’exemplaire : ")
    prenom =prenom.lower()

    while True:
        t_sanction = input("Entrez le type de la sanction (retard, endommagement ou lesdeux) : ")
        if(t_sanction== 'retard' or t_sanction=='endommagement' or t_sanction=='lesdeux'):
            break
        else:
            print(("erreur"))
            continue 
    if t_sanction == 'retard':
        retard = int(input("Entrez le nombre de jours de retard : "))
        date_av_pret= date.today() + timedelta(days=retard)
        sql = "Update Pret Set nb_jours =%i , t_sanction ='%s' Where Pret.id=%i And Pret.date_pret=TO_DATE('%s','YYYYMMDD') And Pret.nom='%s' And Pret.prenom='%s' And Pret.mail='%s';"% (retard,t_sanction,ident,datep,nom,prenom,mail)
        try:
            cur.execute(sql)
            conn.commit()
        except psycopg2.DataError as e:
            conn.rollback()
            print("\n   Modification annulée")
            print("Message système :", e)
            return
        except psycopg2.IntegrityError as e:
            print("\n   Modification annulée")
            print("Message système :", e)
            conn.rollback()
            return
        sql = "Update Adherent Set date_avant_nouvel_emprunt ='%s', droit_pret='f' WHERE nom='%s' And prenom='%s' And mail='%s';"% (date_av_pret,nom,prenom,mail)
        try:
            cur.execute(sql)
            conn.commit()
            print("\n   Modification effectuées")
        except psycopg2.DataError as e:
            conn.rollback()
            print("\n   Modification annulée")
            print("Message système :", e)
            return
        except psycopg2.IntegrityError as e:
            print("\n   Modification annulée")
            print("Message système :", e)
            conn.rollback()
            return

    if t_sanction == 'endommagement':
        while True:
            etat = input("Entrez le nouvel etat: 'bon'ou 'abime' ou 'perdu' : ")
            if( etat =='bon' or etat =='abime' or etat =='perdu'):
                break
            else:
                print(("erreur"))
                continue 
        a_payer = int(input("Entrez le montant à payer pour la sanction : "))
        sql = "Update Pret Set nv_etat ='%s' , a_payer =%i , t_sanction ='%s' Where Pret.id=%i And Pret.date_pret=TO_DATE('%s','YYYYMMDD') And Pret.nom='%s' And Pret.prenom='%s' And Pret.mail='%s';"% (etat, a_payer, t_sanction,ident, datep, nom, prenom, mail)
        try:
            cur.execute(sql)
            conn.commit()
            print("\n   Modification effectuées")
        except psycopg2.DataError as e:
            conn.rollback()
            print("\n   Modification annulée")
            print("Message système :", e)
            return
        except psycopg2.IntegrityError as e:
            print("\n   Modification annulée")
            print("Message système :", e)
            conn.rollback()
            return
        sql = "Update Adherent Set droit_pret='f' WHERE nom='%s' And prenom='%s' And mail='%s';"% (nom,prenom,mail)
        try:
            cur.execute(sql)
            conn.commit()
            print("\n   Modification effectuées")
        except psycopg2.DataError as e:
            conn.rollback()
            print("\n   Modification annulée")
            print("Message système :", e)
            return
        except psycopg2.IntegrityError as e:
            print("\n   Modification annulée")
            print("Message système :", e)
            conn.rollback()
            return

    if t_sanction == 'lesdeux':
        retard = int(input("Entrez le nombre de jours de retard : "))
        while True:
            etat = input("Entrez le nouvel etat: ou 'bon'ou 'abime' ou 'perdu' : ")
            if( etat =='bon' or etat =='abime' or etat =='perdu'):
                break
            else:
                print(("erreur"))
                continue 
        a_payer = int(input("Entrez le montant à payer pour la sanction : "))
        sql = "Update Pret Set nb_jours =%i ,nv_etat ='%s' , a_payer =%i ,t_sanction ='%s' Where Pret.id=%i And Pret.date_pret=TO_DATE('%s','YYYYMMDD') And Pret.nom='%s' And Pret.prenom='%s' And Pret.mail='%s';"% (retard,etat, a_payer, t_sanction,ident, datep, nom, prenom, mail)
        try:
            cur.execute(sql)
            conn.commit()
            print("\n   Modification effectuées")
        except psycopg2.DataError as e:
            conn.rollback()
            print("\n   Modification annulée")
            print("Message système :", e)
            return
        except psycopg2.IntegrityError as e:
            print("\n   Modification annulée")
            print("Message système :", e)
            conn.rollback()
            return
        sql = "Update Adherent Set date_avant_nouvel_emprunt ='%s', droit_pret='f' WHERE nom='%s' And prenom='%s' And mail='%s';"% (date_av_pret,nom,prenom,mail)
        try:
            cur.execute(sql)
            conn.commit()
            print("\n   Modification effectuées")
        except psycopg2.DataError as e:
            conn.rollback()
            print("\n   Modification annulée")
            print("Message système :", e)
            return
        except psycopg2.IntegrityError as e:
            print("\n   Modification annulée")
            print("Message système :", e)
            conn.rollback()
            return

def modifFilm(conn): 
    cur = conn.cursor()

    print("Voici les films actuels : \n")
    Afficher.afficherFilm(conn)

    code = int(input("\nEntrez le code du film à modifier : "))
    choice = 1
    while choice != 0:
        print ("\n\nPour modifier le code classification du film, entrez 1")
        print ("Pour modifier le synopsis du film, entrez 2")
        print ("Pour sortir, entrez 0")

        while True:
            try:
                choice = int(input("Votre choix : "))
                if(choice<0 or choice>2):
                    print("--Le chiffre n'est pas compris entre 0 et 2 veuillez reéssayer--")
                    continue
                else:
                    break
            except:
                print("--Veuillez renseigner un entier--")
                continue
        if choice == 1:
            nouveau_code = int(input("Veuillez entrer le nouveau code classfication du film: "))
            sql = "UPDATE Ressource SET code_classification ='%i' WHERE code ='%i';" %(nouveau_code, code)
            try:
                cur.execute(sql)
                conn.commit()
                print("\n   Modification effectuées")
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Modification annulée")
                print("Message système :", e)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Modification annulée")
                print("Message système :", e)
                conn.rollback()
                return
    
        if choice == 2:
            synopsis = input("Veuillez entrer le nouveau synospsis du film: ")
            sql = "UPDATE Film SET synopsis ='%s' WHERE Film.code ='%i';" % (synopsis, code)
            try:
                cur.execute(sql)
                conn.commit()
                print("\n   Modification effectuées")
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Modification annulée")
                print("Message système :", e)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Modification annulée")
                print("Message système :", e)
                conn.rollback()
                return


def modifLivre(conn):
    cur = conn.cursor()

    print("Voici les films actuels : \n")
    Afficher.afficherLivre(conn)

    code = int(input("\nEntrez le code du livre à modifier : "))
    choice = 1
    while choice != 0:
        print ("\n\nPour modifier le code classification du livre, entrez 1")
        print ("Pour modifier le résumé du livre, entrez 2")
        print ("Pour sortir, entrez 0")

        while True:
            try:
                choice = int(input("Votre choix : "))
                if(choice<0 or choice>2):
                    print("--Le chiffre n'est pas compris entre 0 et 2 veuillez reéssayer--")
                    continue
                else:
                    break
            except:
                print("--Veuillez renseigner un entier--")
                continue
        if choice == 1:
            nouveau_code = int(input("Veuillez entrer le nouveau code classfication du livre: "))
            sql = "UPDATE Ressource SET code_classification ='%i' WHERE code ='%i';" % (nouveau_code, code)
            try:
                cur.execute(sql)
                conn.commit()
                print("\n   Modification effectuées")
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Modification annulée")
                print("Message système :", e)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Modification annulée")
                print("Message système :", e)
                conn.rollback()
                return
        if choice == 2:
            res = input("Veuillez entrer le nouveau résumé du livre : ")
            sql = "UPDATE Livre SET resum ='%s' WHERE livre.code ='%s';" % (res, code)
            try:
                cur.execute(sql)
                conn.commit()
                print("\n   Modification effectuées")
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Modification annulée")
                print("Message système :", e)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Modification annulée")
                print("Message système :", e)
                conn.rollback()
                return



def modifEnregistrement(conn):
    cur = conn.cursor()

    print("Voici les films actuels : \n")
    Afficher.afficherEnregistrement(conn)

    code = int(input("\nEntrez le code de l'enregistrement musical à modifier : "))
    choice = 1
    while choice != 0:
        print ("\n\nPour modifier le code classification de l'enregistrement musical, entrez 1")
        print ("Pour sortir, entrez 0")

        while True:
            try:
                choice = int(input("Votre choix : "))
                if(choice<0 or choice>1):
                    print("--Le chiffre n'est pas compris entre 0 et 1 veuillez reéssayer--")
                    continue
                else:
                    break
            except:
                print("--Veuillez renseigner un entier--")
                continue
        if choice == 1:
            nouveau_code = int(input("Veuillez entrer le nouveau code classfication de l'enregistrement musical: "))
            sql = "UPDATE Ressource SET code_classification ='%i' WHERE code ='%i';" % (nouveau_code, code)
            try:
                cur.execute(sql)
                conn.commit()
                print("\n   Modification effectuées")
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Modification annulée")
                print("Message système :", e)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Modification annulée")
                print("Message système :", e)
                conn.rollback()
                return

def modifMembre(conn): 
    cur = conn.cursor()

    print("Voici les membres actuels : \n")
    Afficher.AfficherMembre(conn)

    nom = input("\nEntrez le nom du membre à modifier : ")
    prenom = input("\nEntrez le prenom du membre à modifier : ")
    mail = input("\nEntrez le mail du membre à modifier : ")
    choice = 1
    while choice != 0:
        print ("\n\nPour modifier l'adresse du membre, entrez 1")
        print ("Pour sortir, entrez 0")

        while True:
            try:
                choice = int(input("Votre choix : "))
                if(choice<0 or choice>1):
                    print("--Le chiffre n'est pas compris entre 0 et 1 veuillez reéssayer--")
                    continue
                else:
                    break
            except:
                print("--Veuillez renseigner un entier--")
                continue
        if choice == 1:
            adresse = input("Veuillez entrer le nouveau numéro de téléphone de l'adhérent : ")
            sql = "UPDATE Utilisateur SET adresse='%s' WHERE mail='%s' AND nom='%s' AND prenom='%s' ;" % (adresse, mail, nom, prenom)
            try:
                cur.execute(sql)
                conn.commit()
                print("\n   Modification effectuées")
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Modification annulée")
                print("Message système :", e)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Modification annulée")
                print("Message système :", e)
                conn.rollback()
                return

def modifAdherent(conn):
    cur = conn.cursor()

    print("Voici les adherents actuels : \n")
    Afficher.AfficherAdherent(conn)

    nom = input("\nEntrez le nom de l'adhérent à modifier : ")
    prenom = input("\nEntrez le prenom de l'adhérent à modifier : ")
    mail = input("\nEntrez le mail de l'adhérent à modifier : ")
    choice = 1
    while choice != 0:
        print ("\n\nPour modifier le numéro de téléphone de l'adhérent, entrez 1")
        print ("Pour modifier l'adresse de l'adhérent, entrez 2")
        print ("Pour modifier l'adhésion de l'adhérent, entrez 3")
        print ("Pour redonner le droit de pret à un adhérent, entrez 4")
        print ("Pour remettre à null la date avant emprunt de l'adhérent, entrez 5")
        print ("Pour sortir, entrez 0")

        while True:
            try:
                choice = int(input("Votre choix : "))
                if(choice<0 or choice>5):
                    print("--Le chiffre n'est pas compris entre 0 et 5 veuillez reéssayer--")
                    continue
                else:
                    break
            except:
                print("--Veuillez renseigner un entier--")
                continue
        if choice == 1:
            telephone = int(input("Veuillez entrer le nouveau numéro de téléphone de l'adhérent : "))
            sql = "UPDATE Adherent SET telephone=%i WHERE mail='%s' AND nom='%s' AND prenom='%s' ;" % (telephone, mail, nom, prenom)
            try:
                cur.execute(sql)
                conn.commit()
                print("\n   Modification effectuées")
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Modification annulée")
                print("Message système :", e)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Modification annulée")
                print("Message système :", e)
                conn.rollback()
                return
        if choice == 2:
            adresse = input("Veuillez entrer la nouvelle adresse de l'adhérent : ")
            sql = "UPDATE Utilisateur SET adresse='%s' WHERE mail='%s' AND nom='%s' AND prenom='%s' ;" % (adresse, mail, nom, prenom)
            try:
                cur.execute(sql)
                conn.commit()
                print("\n   Modification effectuées")
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Modification annulée")
                print("Message système :", e)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Modification annulée")
                print("Message système :", e)
                conn.rollback()
                return
        if choice == 3:
            adhesion = int(input("Veuillez entrer le nouveau statut de l'adhésion de l'adhérent (0|1) : "))
            sql = "UPDATE Adherent SET adhesion='%s' WHERE mail='%s' AND nom='%s' AND prenom='%s' ;" % (adhesion, mail, nom, prenom)
            try:
                cur.execute(sql)
                conn.commit()
                print("\n   Modification effectuées")
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Modification annulée")
                print("Message système :", e)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Modification annulée")
                print("Message système :", e)
                conn.rollback()
                return
        if choice == 4:
            sql = "Update Adherent Set droit_pret='t' WHERE nom='%s' And prenom='%s' And mail='%s';"% (nom,prenom,mail)
            try:
                cur.execute(sql)
                conn.commit()
                print("\n   Modification effectuées")
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Modification annulée")
                print("Message système :", e)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Modification annulée")
                print("Message système :", e)
                conn.rollback()
                return
        if choice == 5:
            sql = "Update Adherent Set date_avant_nouvel_emprunt =%s WHERE nom='%s' And prenom='%s' And mail='%s';"% ('NULL',nom,prenom,mail)
            try:
                cur.execute(sql)
                conn.commit()
                print("\n   Modification effectuées")
            except psycopg2.DataError as e:
                conn.rollback()
                print("\n   Modification annulée")
                print("Message système :", e)
                return
            except psycopg2.IntegrityError as e:
                print("\n   Modification annulée")
                print("Message système :", e)
                conn.rollback()
                return

